package service

import (
	"encoding/json"
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"gitlab.com/ghostbutler/tool/service/device"
	"time"
)

type Database struct {
	isRunning bool

	// mongo instance
	mongo *mgo.Session

	// database
	Database string

	// sensor collection
	SensorCollection string
}

func BuildDatabase(configuration *Configuration) (*Database, error) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    configuration.MongoDB.Hostname,
		Timeout:  10 * time.Second,
		Database: configuration.MongoDB.Authentication.Database,
		Username: configuration.MongoDB.Authentication.UserName,
		Password: configuration.MongoDB.Authentication.Password,
	}
	if mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo); err != nil {
		return nil, err
	} else {
		mongoSession.SetMode(mgo.Monotonic, true)
		mongoSession.SetSafe(&mgo.Safe{WMode: "majority"})
		database := &Database{
			isRunning:        true,
			mongo:            mongoSession,
			Database:         configuration.MongoDB.Database,
			SensorCollection: configuration.MongoDB.Collection.Sensor,
		}
		return database, nil
	}
}

func (database *Database) Close() {
	database.isRunning = false
	database.mongo.Close()
}

func (database *Database) RequestStatus(_type,
	name string) ([]byte, error) {
	startTime := time.Now()

	session := database.mongo.Clone()
	collection := session.DB(database.Database).C(database.SensorCollection)

	fmt.Println("Got status request for", name, "(", _type, ")")

	sensorData := &device.SensorData{}
	if err := collection.Find(bson.M{
		"data.name": name,
		"type":      _type,
	}).Sort("-time").One(sensorData); err == nil {
		fmt.Println( sensorData.Data )
		if err := sensorData.ParseData(); err == nil {
			export, _ := json.Marshal(sensorData)
			fmt.Println("Took",
				time.Now().Sub(startTime))
			return export, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
