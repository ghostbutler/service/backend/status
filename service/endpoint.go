package service

import (
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"net/http"
)

// define your services ids, must start from last built in index
const (
	// login based on an OTP
	// use to contact the service from outside of the
	// internal zone
	// delivers a session ID valid for a given period
	APIServiceV1StatusGet = common.APIServiceType(iota + common.APIServiceBuiltInLast)
)

// declare how you want your endpoints to be accessed to
// Path is the split version of URL.Path by '/' character. So to express /api/v1/first, you'll put here
// ["api", "v1", "first"]
// Method is the HTTP method to use to access this endpoint
// Callback is a function of type common.EndpointFunction which will be called when an HTTP request triggers an endpoint
var DirectoryAPIService = map[common.APIServiceType]*common.APIEndpoint{
	// get status
	APIServiceV1StatusGet: {
		Path:                    []string{"api", "v1", "status", "get"},
		Method:                  "GET",
		Description:             "get a status from type/name",
		Callback:                CallbackAPIServiceV1StatusGet,
		IsMustProvideOneTimeKey: false,
	},
}

// external login with username/password + OTP
func CallbackAPIServiceV1StatusGet(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	srv := handle.(*Service)
	if err := request.ParseForm(); err == nil {
		if result, err := srv.database.RequestStatus(request.FormValue("type"),
			request.FormValue("name")); err == nil {
			fmt.Println("got request for",
				request.FormValue("type"),
				"/",
				request.FormValue("name"),
				":",
				string(result))
			_, _ = rw.Write(result)
		} else {
			rw.WriteHeader(http.StatusInternalServerError)
			return http.StatusInternalServerError
		}
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}
	return http.StatusOK
}
